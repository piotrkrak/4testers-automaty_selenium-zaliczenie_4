import time

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

from selenium.webdriver.common.by import By
from pages.arena.login_page import LoginPage

email = 'administrator@testarena.pl'
project_name = '4Test-PK59'
project_prefix = '229P'


@pytest.fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    login_page = LoginPage(browser)
    login_page.visit()
    login_page.login(email, 'sumXQQ72$L')

    yield browser

    browser.close()


def test_chrome_selenium_add_project(browser):

    user_email = browser.find_element(By.CSS_SELECTOR, ".user-info small").text
    assert user_email == 'administrator@testarena.pl'

    # Weryfikacja czy tytuł otwartej strony zawiera w sobie 'TestArena'
    assert 'TestArena' in browser.title

    # Przejscie do panelu admnistracyjnego
    browser.find_element(By.CSS_SELECTOR, '[title=Administracja]').click()
    assert 'Projekty' in browser.title

    # Przejdz do dodawania projektu
    label_text = browser.find_element(By.LINK_TEXT, 'DODAJ PROJEKT')
    label_text.click()
    assert 'Dodaj projekt' in browser.title

    # Obsługa formularza Dodaj projekt
    browser.find_element(By.CSS_SELECTOR, '#name').send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, '[name=prefix]').send_keys(project_prefix)
    browser.find_element(By.CSS_SELECTOR, '[value=Zapisz]').click()

    # Znajdż utworzony projekt
    browser.find_element(By.CSS_SELECTOR, '.item2').click()
    browser.find_element(By.CSS_SELECTOR, '#search').send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    # Weryfikacja wyników wyszukiwania
    assert browser.find_element(By.LINK_TEXT, project_name)
