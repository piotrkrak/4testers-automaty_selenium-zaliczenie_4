import time

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver import Edge
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from webdriver_manager.chrome import ChromeDriverManager

from selenium.webdriver.common.by import By

from pages.arena.home_page import HomePage
from pages.arena.login_page import LoginPage
from pages.arena.project_page import ProjectPage

email = 'administrator@testarena.pl'


@pytest.fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    login_page = LoginPage(browser)
    login_page.visit()
    login_page.login(email, 'sumXQQ72$L')

    yield browser

    browser.close()


def test_chrome_selenium_login(browser):
    time.sleep(4)

    user_email = browser.find_element(By.CSS_SELECTOR, ".user-info small").text
    assert user_email == 'administrator@testarena.pl'

    # Weryfikacja czy tytuł otwartej strony zawiera w sobie 'TestArena'
    assert 'TestArena' in browser.title


def test_chrome_selenium_logout(browser):
    home_page = HomePage(browser)
    home_page.logout()
    assert browser.current_url == 'http://demo.testarena.pl/zaloguj'


def test_chrome_selenium_messages(browser):
    browser.find_element(By.CSS_SELECTOR, '.icon_mail').click()
    n = 1
    while browser.find_element(By.CSS_SELECTOR, '#j_msgIsBusy').is_displayed():
        time.sleep(1)
        n = n + 1
        print(n)
    print('2')


def test_add_message(browser):
    browser.find_element(By.CSS_SELECTOR, '.top_messages').click()
    wait = WebDriverWait(browser, 10)

    # tupla
    message_area = (By.CSS_SELECTOR, '#js_msgContent')
    wait.until(expected_conditions.element_to_be_clickable(message_area))


def test_find_projekt_kamil(browser):
    home_page = HomePage(browser)

    home_page.admin_projekty()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'

    project_page = ProjectPage(browser)
    project_page.project_search('Kamil')
    project_page.veryfied_projects_found()


