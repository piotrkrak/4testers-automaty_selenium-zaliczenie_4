from selenium.webdriver.common.by import By


class ProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def project_search(self, project_name_to_search):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(project_name_to_search)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def veryfied_projects_found(self):
        found_projects = self.browser.find_elements(By.CSS_SELECTOR, 'tbody tr')
        assert len(found_projects) > 0
